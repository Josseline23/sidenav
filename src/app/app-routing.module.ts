import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AddUserComponent} from './add-user/add-user.component';
import { DeleteUserComponent } from './delete-user/delete-user.component';
import {ViewUserComponent} from './view-user/view-user.component';
import {UpdateUserComponent} from './update-user/update-user.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [

  {
    path: '',
    component: HomeComponent
  },

  {
    path: 'Add-User',
    component: AddUserComponent 
  },
  {
    path: 'View-User',
    component: ViewUserComponent 
  },
  {
    path: 'Update-User',
    component: UpdateUserComponent
  },
  {
    path: 'Delete-User',
    component: DeleteUserComponent
  },

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
